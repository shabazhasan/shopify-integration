from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
import requests
import json
from django.conf import settings
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect

from forms import SignUpForm

headers = {"Content-type": "application/json"}
# username = '6254224e2734182ab5fb2c64a8da3036'
# password = 'ac0a71c6a9e42f4ee2acdcf83b9b1f40'
# mystore = 'mystore1920'
base_url = 'https://{}:{}@{}.myshopify.com'.format(settings.API_KEY, settings.API_SECRET, settings.SHOP)


# query_params = {"inventory_behavior": "decrement_obeying_policy"}



def order_product(request):

    variant_id = request.GET.get('variant_id')
    query_params = {"inventory_behavior": "decrement_obeying_policy"}
    
    data = {
    "order": {
        "line_items": [
                {
                "variant_id": variant_id,
                "quantity":1
                }
            ]
        }
    }

    response = requests.post('{}/admin/orders.json'.format(base_url),
                      headers=headers, data=json.dumps(data), params=query_params)

    if response.status_code is not 201:
        return JsonResponse({"response":"Unable to create order"})

    
    return JsonResponse({"response":"order created successfully"})




@login_required
def home(request):
    #product retrieve from shopify

    response = requests.get('{}/admin/products.json'.format(base_url),
                     headers=headers)
    
    if response.status_code is not 200:
        return HttpResponse("failed")

    list_of_products = json.loads((response.content).decode("utf-8"))
    
    return render(request, 'home.html', context={'products': list_of_products['products']})


def signup(request):

    if request.method == 'POST':
        form = SignUpForm(request.POST)
        
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    
    else:
        form = SignUpForm()
    
    return render(request, 'signup.html', {'form': form})
